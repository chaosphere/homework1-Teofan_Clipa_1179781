#!/usr/bin/env bash

# A small script to automate the execution of the evaluation of the runs

echo "Valutazione StoplistStemmerBM25 >> results1"
trec_eval -q -m all_trec Ground_Truth.txt StoplistStemmerBM25.txt >> results1_whoosh.txt
trec_eval -q -m map Ground_Truth.txt StoplistStemmerBM25.txt >> results1_whoosh_map.txt
trec_eval -q -m Rprec Ground_Truth.txt StoplistStemmerBM25.txt >> results1_whoosh_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt StoplistStemmerBM25.txt >> results1_whoosh_prec@10.txt

echo "Valutazione StoplistStemmerTF_IDF >> results2"
trec_eval -q -m all_trec Ground_Truth.txt StoplistStemmerTF_IDF.txt >> results2_whoosh.txt
trec_eval -q -m map Ground_Truth.txt StoplistStemmerTF_IDF.txt >> results2_whoosh_map.txt
trec_eval -q -m Rprec Ground_Truth.txt StoplistStemmerTF_IDF.txt >> results2_whoosh_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt StoplistStemmerTF_IDF.txt >> results2_whoosh_prec@10.txt

echo "Valutazione NoStoplistStemmerBM25 >> results3"
trec_eval -q -m all_trec Ground_Truth.txt NoStoplistStemmerBM25.txt >> results3_whoosh.txt
trec_eval -q -m map Ground_Truth.txt NoStoplistStemmerBM25.txt >> results3_whoosh_map.txt
trec_eval -q -m Rprec Ground_Truth.txt NoStoplistStemmerBM25.txt >> results3_whoosh_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt NoStoplistStemmerBM25.txt >> results3_whoosh_prec@10.txt

echo "Valutazione NoStoplistNoStemmerTF_IDF.txt >> results4"
trec_eval -q -m all_trec Ground_Truth.txt NoStoplistNoStemmerTF_IDF.txt >> results4_whoosh.txt
trec_eval -q -m map Ground_Truth.txt NoStoplistNoStemmerTF_IDF.txt >> results4_whoosh_map.txt
trec_eval -q -m Rprec Ground_Truth.txt NoStoplistNoStemmerTF_IDF.txt >> results4_whoosh_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt NoStoplistNoStemmerTF_IDF.txt >> results4_whoosh_prec@10.txt

echo "Valutazione delle run completata!"
