# Contenuti

Questa cartella contiene le valutazioni delle run eseguite con *whoosh* oltre che
i file dei **Topic** e del **Ground truth**. Dato che le prestazioni ottenute
sono molto inferiori alle run eseguite con **Terrier**, soprattutto per quanto
riguarda il modello *TF_IDF*, ho preferito inserire nella relazione solamente
i risultati delle ultime.

Lo script *Execute_evaluation.sh* si occupa di valutare le run e di creare i file
con i risultati. I nomi dei file dovrebbero essere auto-esplicativi.

In ogni caso, si può eseguire i test statistici anche su queste run, modificando
il path nei file *Matlab* in modo che puntino a questi risultati invece che a
quelli presenti dentro la cartella *Terrier*.
