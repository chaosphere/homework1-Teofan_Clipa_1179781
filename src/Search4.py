# Importiamo le funzioni utili per la creazione degli indici
import Functions as fun
import os, os.path
from whoosh.filedb.filestore import FileStorage
from whoosh import index
from whoosh import scoring
from whoosh import qparser
from whoosh.qparser import QueryParser
from whoosh.fields import Schema, STORED, ID, TEXT
import time
from math import *
import gensim.parsing.preprocessing as pre

'''
    Stesso preprocessing usato per la creazione dell'indice, va usato anche per
    la query.
'''
def preprocess(sentence):
    # uso dei filtri personalizzati, che permette di eseguire solo le operazioni volute
    CUSTOM_FILTERS = [lambda x: x.lower(), pre.strip_tags, pre.strip_punctuation, pre.strip_multiple_whitespaces, pre.strip_non_alphanum, pre.strip_numeric]
    return pre.preprocess_string(sentence, CUSTOM_FILTERS)


# Path utili che servono nel seguito
topics_path="../res/Topics.txt"
resources_path="../res/"
index_path="HW1_indexes"

# Per prima cosa, leggiamo il file dei topic
topics = fun.read_topics(topics_path)

# Apriamo l'indice creato, per ogni topic, dobbiamo preprocessare il titolo e poi effettuare la ricerca
ix = index.open_dir(index_path, indexname="NoStoplistNoStemmer")

# definiamo il file dove stamperemo i risultati della query
filename = open(resources_path + "NoStoplistNoStemmerTF_IDF.txt", mode='w')

# serve un oggetto parser della query, come nell'esempio visto sui Jupyter Notebook,
# specifico che voglio anche i risultati con solo alcune delle parole della query
# usando cioè un OR tra le parole, e non un AND.
qp = QueryParser("content", schema=ix.schema, group=qparser.OrGroup)

# in questo caso bisogna usare la pesatura TF*IDF, lo specifico qui
with ix.searcher(weighting=scoring.TF_IDF()) as searcher:
    # eseguiamo una ricerca per ogni topic presente nel file
    for t in topics:
        qPreproc = preprocess(t.title)
        query = ""
        for word in qPreproc:
            query = query + " " + word
        q = qp.parse(query)
        # prendo al massimo 10000 risultati
        results = searcher.search(q, limit=10000)
        # Ora possiamo scrivere i risultati nel file, seguendo la convenzione di TREC
        for hit in results:
            line = str(t.topic_id) + " Q0 " +  hit["path"].strip() + " " + str(hit.pos) + " " + str(hit.score) + " NoStoplistNoStemmerTF_IDF\n"
            filename.write(line)

# chiudiamo il file aperto in scrittura
filename.close()
