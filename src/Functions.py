# Funzioni utili per processare file di tipo TREC
from gensim.parsing.preprocessing import preprocess_string
import re
import io
import glob
import string
from smart_open import smart_open
from gensim.parsing.porter import PorterStemmer

# Let's define a class to pre-process words: remove stop words and apply the stemmer

# function to remove tags from a string
def remove_tags(raw_text):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_text)
    return cleantext

# re-define a decode method to ignore UTF-8 errors
# this is required because TIPSTER files contain some encoding errors.
def decode(s, encoding="ascii", errors="ignore"):
    return s.decode(encoding=encoding, errors=errors)

# a TREC doc objects
class TREC_Doc(object):

    # The class "constructor" - It's actually an initializer
    def __init__(self, doc_id, title, content):
        self.doc_id = doc_id
        self.title = title
        self.content = content

    def getTRECid(self):
        return self.doc_id

    def getTitle(self):
        return self.title

    def getContent(self):
        return self.content


# yield one doc in memory at a time
class MyDocuments(object):
    def __init__(self, path, preprocess):
        self.collection_path = path
        self.p = preprocess

    def __iter__(self):
        for f in glob.iglob(self.collection_path + '**/*', recursive=True):
            indoc = False
            intitle = False
            intext = False

            # we read the file as a byte stream and then encode in UTF-8 by ignoring encoding errors
            for line in smart_open(f, 'rb'):
                line = decode(line)
                # skip not-UTF-8 valid characters
                if line.find("</DOC>")!=-1:
                    indoc = False
                    intitle = False
                    intext = False
                    # we return a TREC doc with title and content divided into fields. We remove all extra white spaces.
                    yield TREC_Doc(docno, self.p(title), self.p(text))
                # find the beginning of a doc
                elif indoc:
                    if (line.find("<TI>")!=-1 and line.find("</TI>")!=-1):
                        intitle = False
                        title = remove_tags(line).rstrip()
                    elif (line.find("</TI>") !=-1 or line.find("</HEADLINE>") !=-1 or line.find("</HEADER>") !=-1):
                        intitle = False
                    elif (line.find("<TI>")!=-1 or line.find("<HEADLINE>")!=-1 or intitle):
                        intitle = True
                        title = title + " " + re.sub(' +', ' ', remove_tags(line).rstrip())
                    elif (line.find("</TEXT>") !=-1):
                        intext = False
                    elif (line.find("<TEXT>") !=-1 or intext):
                        intext = True
                        text = text + " " + re.sub(' +', ' ', remove_tags(line).rstrip())
                    elif line.find("<DOCNO>")!=-1:
                        docno = remove_tags(line)
                        #print("processing docno: " + docno)
                elif line.find("<DOC>")!=-1:
                    docno = ""
                    title = ""
                    text = ""
                    indoc = True

# Leggiamo i file dei topic

# Definisco una classe topic per memorizzare l'ID del topic e il titolo (information need)
class Topic(object):
    def __init__(self, id, title):
        # servirà per memorizzare l'id del topic
        self.topic_id = id
        # servirà per memorizzare la ricerca da eseguire
        self.title = title

# Funzione che legge i topic e restituisce una lista di topic con ID e titolo
def read_topics(topics_path):
    # ritorno un array di topic
    topics = []
    # apriamo il file in lettura
    topic_file = open(topics_path, mode='r')
    t_id = None
    t_title = None
    for line in topic_file:
        if line.find("<num>")!=-1:
            # se nella linea corrente troviamo il tag num, allora possiamo leggere l'ID del topic
            index = line.find("Number: ")
            # salviamo l'ID del topic in una variabile
            t_id = int(line[index + 8 :])
        elif line.find("<title>")!=-1:
            # Se invece siamo sulla riga <title> allora dobbiamo leggere la query
            t_title = line[line.find("<title>") + 8 :]
        if t_id != None and t_title != None:
            # in questo caso, ho appena trovato un topic, lo devo quindi aggiungere
            # creo un nuovo oggetto Topic
            t = Topic(id=t_id, title=t_title)
            topics.append(t)
            # quindi riazzeriamo le variabili per cercare il prossimo topic
            t_id = None
            t_title = None
    # chiudo il file dei topic aperto in lettura
    topic_file.close()
    # ritorno una lista di oggetti Topic
    return topics
