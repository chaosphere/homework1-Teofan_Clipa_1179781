# Importiamo le funzioni utili per la creazione degli indici
import Functions as fun
import os, os.path
from whoosh.filedb.filestore import FileStorage
from whoosh import index
from whoosh.fields import Schema, STORED, ID, TEXT
import time
from math import *
import gensim.parsing.preprocessing as pre

'''
    Definisco una funzione di preprocessing in base all'indice richiesto. In questo
    caso l'indice va fatto sia senza l'uso dello stemmer che senza la rimozione delle
    stopword. Rimuovo quindi spazi bianchi, caratteri non alfanumerici, tags,
    punteggiatura e basta.
'''
def preprocess(sentence):
    # uso dei filtri personalizzati, che permette di eseguire solo le operazioni volute
    CUSTOM_FILTERS = [lambda x: x.lower(), pre.strip_tags, pre.strip_punctuation, pre.strip_multiple_whitespaces, pre.strip_non_alphanum, pre.strip_numeric]
    return pre.preprocess_string(sentence, CUSTOM_FILTERS)

# path della collezione di documenti
collection_path="../Docs/Gasta"
# path dove viene salvato l'indice
index_path="HW1_indexes"

# Schema usato dal prof. Silvello
schema = Schema(title=TEXT(stored=True), content=TEXT,
                path=ID(stored=True))

exists = index.exists_in(index_path)

if not exists:
    if not os.path.exists(index_path):
        os.mkdir(index_path)

    storage = FileStorage(index_path)

    # Crea l'oggetto indice
    ix = storage.create_index(schema, indexname="NoStoplistNoStemmer")
else:
    ix = index.open_dir(index_path, indexname="NoStoplistNoStemmer")

# All'oggetto MyDocuments passo sia il path dei documenti che la funzione da usare per il preprocessing
corpus_memory_friendly = fun.MyDocuments(path=collection_path, preprocess=preprocess)

writer = ix.writer()

# Creazione dell'indice. Codice del prof. Gianmaria Silvello
print("Indexing ...")
start_time = time.time()
i = 0
for doc in corpus_memory_friendly:
    i = i + 1
    if(i % 20000 == 0):
        print("%d docs indexed in %s seconds ---" % (i, time.time() - start_time))
        writer.commit(merge=False)
        writer = ix.writer()

    writer.add_document(title=doc.getTitle(), content=doc.getContent(),
                    path=doc.getTRECid())

print("FINAL: %d docs indexed in %s seconds ---" % (i, time.time() - start_time))

writer.commit(optimize=True)

print("Index merged and optimized in %s seconds ---" % (time.time() - start_time))
