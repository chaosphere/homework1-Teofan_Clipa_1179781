#!/usr/bin/env bash
# Semplice script per eseguire le 4 run dei modelli.
echo "Inizio delle ricerche dei topic nei 4 casi di interesse in corso..."

python Search1.py
echo "Search1 eseguito correttamente..."
python Search2.py
echo "Search2 eseguito correttamente..."
python Search3.py
echo "Search3 eseguito correttamente..."
python Search4.py
echo "Search4 eseguito correttamente..."

echo "Ricerche completate. I risultati sono stati salvati nella cartella res"
