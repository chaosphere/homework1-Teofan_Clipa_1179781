# Contenuti

In questa cartella sono presenti i file *.properties* di configurazione di
**Terrier**. Il nome dei file dovrebbe essere auto-esplicativo, riflettono il
modo in cui viene creato l'indice.

I file vanno copiati, uno per volta, dentro la cartella *etc* di **Terrier** e
quindi rinominato in *terrier.properties* prima dell'indicizzazione. Gli indici
verrano creati in cartelle differenti denominate come i file *.properties* presenti,
prima di eseguire l'indicizzazione assicurarsi che esistano dentro la cartella
*var* di **Terrier**. Ad esempio, l'indice creato senza l'uso del porter stemmer
e senza rimuovere le stopwords verrà salvato in *var/NoStoplistNoStemmer*.

La carella *Evaluations* contiene invece tutti i file con i risultati ottenuti
dai 4 sistemi analizzati. All'interno è anche presente uno script che esegue la
valutazione in automatico a partire dai file con i risultati delle ricerche.
Questi file vanno copiati dalla cartella *var/results* di **Terrier** e rinominati
coerentemente alla seguente lista:

- StoplistStemmerBM25 >> results1_terrier.txt
- StoplistStemmerTF_IDF >> results2_terrier.txt
- NoStoplistStemmerBM25 >> results3_terrier.txt
- NoStoplistNoStemmerTF_IDF >> results4_terrier.txt

come specificato anche nel file [README.md](../README.md) nel root del repo.
