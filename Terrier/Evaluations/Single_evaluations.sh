#!/usr/bin/env bash 

# A small script to automate the execution of the evaluation of the runs

echo "Valutazione StoplistStemmerBM25 >> results1_*"
trec_eval -q -m map Ground_Truth.txt results1_terrier.txt >> results1_terrier_map.txt
trec_eval -q -m Rprec Ground_Truth.txt results1_terrier.txt >> results1_terrier_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt results1_terrier.txt >> results1_terrier_prec@10.txt

echo "Valutazione StoplistStemmerTF_IDF >> results2_*"
trec_eval -q -m map Ground_Truth.txt results2_terrier.txt >> results2_terrier_map.txt
trec_eval -q -m Rprec Ground_Truth.txt results2_terrier.txt >> results2_terrier_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt results2_terrier.txt >> results2_terrier_prec@10.txt

echo "Valutazione NoStoplistStemmerBM25 >> results3_*"
trec_eval -q -m map Ground_Truth.txt results3_terrier.txt >> results3_terrier_map.txt
trec_eval -q -m Rprec Ground_Truth.txt results3_terrier.txt >> results3_terrier_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt results3_terrier.txt >> results3_terrier_prec@10.txt

echo "Valutazione NoStoplistNoStemmerTF_IDF.txt >> results4_*"
trec_eval -q -m map Ground_Truth.txt results4_terrier.txt >> results4_terrier_map.txt
trec_eval -q -m Rprec Ground_Truth.txt results4_terrier.txt >> results4_terrier_rprec.txt
trec_eval -q -m P.10 Ground_Truth.txt results4_terrier.txt >> results4_terrier_prec@10.txt

echo "Valutazione delle run completata!"
 
